<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->session()->get('token');
        if($token){
            $baseUrl = config('app.base_url');
            $res = Http::withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token
            ])->post($baseUrl.'api/me');
            $decode = json_decode($res);
            $decode->token = $token;
            if(property_exists($decode,'id')){
                return $next($request);
            }
        }
        return redirect()->route('login');
    }
}
