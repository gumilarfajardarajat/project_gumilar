<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'tb_m_client';
    protected $primaryKey = 'client_id';
    public $timestamps = false;
}
