<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'tb_m_project';
    protected $primaryKey = 'project_id';
    public $timestamps = false;
}
