<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>MyProject</title>
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('public/css/sb-admin-2.min.css') }}">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light static-top mb-5 shadow">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">MyProject</a>
            <a class="btn btn-outline-primary" href="{{ route('logout') }}">Logout</a>
        </div>
    </nav>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <form class="d-sm-flex align-items-end">
                    <div class="d-flex align-items-center mr-5">
                        <p>Filter</p>
                    </div>
                    <div class="mr-2 mb-sm-0">
                        <label>Project Name</label>
                        <input type="text" id="project" class="form-control">

                    </div>
                    <div class="mr-2 mb-sm-0">
                        <label>Client</label>
                        <select id="client" class="form-control">
                        </select>
                    </div>
                    <div class="mr-2 mb-sm-0 mb-2">
                        <label>Status</label>
                        <select id="status" class="form-control">
                            <option value="OPEN">OPEN</option>
                            <option value="DOING">DOING</option>
                            <option value="DONE">DONE</option>
                            <option value="" selected>All Status</option>
                        </select>
                    </div>
                    <div>
                        <button type="button" id="btn-search" class="btn btn-primary mr-2">Search</button>
                        <button type="button" id="btn-clear" class="btn border-primary text-primary">Clear</button>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary mr-2 btn-new">New</button>
                    <button type="submit" class="btn btn-danger btn-delete">Delete</button>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th><input type="checkbox" name="check-all"></th>
                                <th>Action</th>
                                <th>Project Name</th>
                                <th>Client</th>
                                <th>Project Start</th>
                                <th>Project End</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header bg-danger text-white">
                        <h5 class="modal-title">Delete File</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this item ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light border-dark" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal modal-info" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>No data checked</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light border-dark" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-form" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form>
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white">
                        <h5 class="modal-title">New</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Project Name</label>
                            <input type="text" class="form-control" name="project_name" >
                        </div>
                        <div class="form-group">
                            <label>Client</label>
                            <select id="form-client" class="form-control" name="client_id" >

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select id="status" class="form-control" name="project_status" >
                                <option value="" disabled selected>Select Status</option>
                                <option value="OPEN">OPEN</option>
                                <option value="DOING">DOING</option>
                                <option value="DONE">DONE</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Project Start</label>
                            <input type="date" class="form-control" value="" name="project_start" >
                        </div>
                        <div class="form-group">
                            <label>Project End</label>
                            <input type="date" class="form-control" value="" name="project_end" >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"
        integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('public/js/sb-admin-2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://momentjs.com/downloads/moment.js"></script>
    <script>
        const baseUrl = @json(url('/'));
        const token = @json(session('token'));
        const defaultURL = 'api/project';
        let dataTableConfig = {
            ajax: {
                url: defaultURL,
                dataSrc: 'data',
                headers:{
                    Authorization: 'Bearer '+token
                }
            },
            searching: false,
            lengthChange: false,
            paging: true,
            pageLength: 5,
            columnDefs: [
                {
                    target: 0,
                    visible: false,
                },
                { "bSortable": false, "aTargets": [ 0, 1, 2 ] },
            ],
            order: [[ 0, 'desc' ]],
            columns: [
                {
                    data: 'project_id'
                },
                {
                    data: null,
                    render: function(data, type) {
                        return '<input type="checkbox" name="check">';
                    },
                    
                },
                {
                    data: null,
                    render: function(data, type) {
                        return '<button type="button" class="btn border-primary text-primary btn-edit">Edit</button>';
                    }
                },
                {
                    data: 'project_name'
                },
                {
                    data: 'client_name'
                },
                {
                    data: 'project_start',
                    render: function(data, type){
                        return moment(data).format('DD MMM YYYY');
                    }
                },
                {
                    data: 'project_end',
                    render: function(data, type){
                        return moment(data).format('DD MMM YYYY');
                    }
                },
                {
                    data: 'project_status'
                },
            ],
        }
        let table = $('table').DataTable(dataTableConfig);

        axios.get('api/client',{
            headers:{
                Authorization: 'Bearer '+token
            }
        }).then((res) => {
            // console.log('client');
            let listClientName = res.data.data;
            let htmlClientName = listClientName.map((item) => {
                return `<option value="${item.client_id}">${item.client_name}</option>`;
            })
            $('#client').html(htmlClientName);
            $('#form-client').html(`<option value="" disabled selected>Select Client</option>`);
            $('#form-client').append(htmlClientName);
            $('#client').append(`<option value="" selected>All Client</option>`);
        })

        $('#btn-search').on('click', function() {
            let project_name = $('#project').val() ? 'project_name=' + $('#project').val() : '';
            let client_id = $('#client').val() ? 'client_id=' + $('#client').val() : '';
            let project_status = $('#status').val() ? 'project_status=' + $('#status').val() : '';
            let list = [];
            project_name && list.push(project_name)
            client_id && list.push(client_id)
            project_status && list.push(project_status)
            let url = 'api/project?' + list.join('&');
            // console.log(url);
            table.ajax.url(url).load();
        });

        $('#btn-clear').on('click', function() {
            $('#client').val('');
            $('#project').val('');
            $('#status').val('');
            table.ajax.url(defaultURL).load();
            table.order( [ 0, 'desc' ] ).draw();
            $('#btn-search').trigger('click');
        });


        jQuery.validator.addMethod("checkdate", function(value, element) {
            console.log(value);
            return this.optional(element) || value > $('.modal-form form [name="project_start"]').val();
        }, "project_end should be greater than project_start");
        

        $('.modal-form form').on('submit', function(e) {
            e.preventDefault();
        }).validate({
            rules:{
                'project_name':{
                    required: true,
                },
                'client_id':{
                    required: true,
                },
                'project_status':{
                    required: true,
                },
                'project_start':{
                    required: true,
                },
                'project_end':{
                    required: true,
                    checkdate: true
                }
            },
            errorClass: 'text-danger',
            submitHandler: function(e){
                let dataId = $('.modal-form form').attr('data-id');
                let formData = new FormData($('.modal-form form')[0]);
                let url = 'api/project';
                $('.modal-form form').find('button[type="submit"]').attr('disabled',true);
                if(dataId > 0){
                    formData.append('_method','put');
                    url = 'api/project/'+dataId;
                }
                axios({
                    method: 'post',
                    url: url,
                    data: formData,
                    headers:{
                        Authorization: 'Bearer '+token
                    }
                }).then((res) => {
                    // console.log(res);
                    $('.modal-form form').find('button[type="submit"]').attr('disabled',false);
                    table.ajax.reload();
                    if(dataId == 0){
                        console.log(dataId);
                        table.order( [ 0, 'desc' ] ).draw();
                    }
                    
                    $('.modal-form').modal('hide');
                });
            }
            
        });

        $('table').on('click', '.btn-edit', function() {
            let data = table.row(this.closest('tr')).data();
            $('.modal-form form [name="client_id"]').val(data.client_id);
            $('.modal-form form [name="project_name"]').val(data.project_name);
            $('.modal-form form [name="project_status"]').val(data.project_status);
            $('.modal-form form [name="project_start"]').val(data.project_start);
            $('.modal-form form [name="project_end"]').val(data.project_end);
            $('.modal-form form').attr('data-id',data.project_id);
            $('.modal-form .modal-header').html(`<h5 class="modal-title">Edit</h5>`);
            $('.modal-form').modal('show');
        });

        $('table').on('change', '[name="check"]', function() {
            var cells = table.cells().nodes();
            let length = $(cells).find(':checkbox:checked').length;
            if(length == table.rows().count()){
                $('[name="check-all"]').prop('checked',true)
            }else{
                $('[name="check-all"]').prop('checked',false)
            }
            
        });

        $('.btn-new').on('click', function() {
            $('.modal-form').modal('show');
            $('form').trigger('reset');
            $('.modal-form .modal-header').html(`<h5 class="modal-title">New</h5>`);
            $('form').attr('data-id',0);
        });

        $('.btn-delete').on('click',function(e){
            var cells = table.cells().nodes();
            let length = $(cells).find(':checkbox:checked').length;
            if(length > 0){
                $('.modal-delete').modal('show');
            }else{
                $('.modal-info').modal('show');
            }
            
        });

        $('[name="check-all"]').on('change',function(){
            var cells = table.cells().nodes();
            table.rows({ page: 'current' }).nodes();
            $(cells).find(':checkbox').prop('checked', $(this).is(':checked'));
            let length = $(cells).find(':checkbox:checked').length;
            if(length > 1){
                $('.modal-delete .modal-body').html(`<p> Are you sure you want to delete all ${length} items </p>`);
            }else{
                $('.modal-delete .modal-body').html(`<p> Are you sure you want to delete this item </p>`);
            }
            
        })
        
        $('.modal-delete form').on('submit',function(e){
            e.preventDefault();
            var cells = table.cells().nodes();
            let listId = $(cells).find(':checkbox:checked').map(function(){
                let data = table.row($(this).closest('tr')[0]).data();
                return data.project_id;
            }).get();

            let formData = new FormData();
            formData.append('list',listId);
            formData.append('_method','delete');
            $(this).find('button[type="submit"]').attr('disabled',true);
            axios({
                method: 'post',
                url: 'api/project',
                headers:{
                    Authorization: 'Bearer '+token
                },
                data: formData
            }).then((res) => {
                // console.log(res);
                table.ajax.reload();
                $('[name="check-all"]').prop('checked',false);
                $('.modal-delete').modal('hide');
                $(this).find('button[type="submit"]').attr('disabled',false);
            });

        });

        table.on( 'draw', function () {
            let length = table.rows().count();
            if(length > 5){
                // console.log('show paginate');
                $('#DataTables_Table_0_paginate').show();
            }else{
                // console.log('hide paginate');
                $('#DataTables_Table_0_paginate').hide();
            }
        } );

        

    </script>
</body>

</html>