## Intro
This is project_gumilar for Arkamaya test.
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://www.arkamaya.co.id/assets/images/arka/logo-arkamaya-flat.png" width="100" alt="Laravel Logo"></a></p>
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>



## Prerequisites
PHP >= 8.0
XAMPP >= 8.2.0


## Setup

1) Open Mysql Database create schema db_project_gumilar

2)  Go to xampp htdocs directory  “C:\xampp\htdocs” and inside it Clone project_gumilar

```bash
git clone https://gitlab.com/gumilarfajardarajat/project_gumilar
```
 

3) After the application has been cloned, cd into project_gumilar and
Install using these commands:

```bash
    cd project_gumilar
    composer install
    composer update
    npm install
```

4) make migration :


```bash
    php artisan migrate --seed
```
5) After make migration, use this account to get an access
```bash
    email: basquiat@gmail.com
    password: 12345678
```

