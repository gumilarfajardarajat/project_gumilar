<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function login(Request $request){
        $token = $request->session()->get('token');
        if($token){
            $baseUrl = config('app.base_url');
            $res = Http::withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token
            ])->post($baseUrl.'api/me');
            $decode = json_decode($res);
            $decode->token = $token;
            if(property_exists($decode,'id')){
                return redirect()->back();
            }
        }
        return view('login');
        
    }

    public function logout(Request $request){
        $token = $request->session()->get('token');
        if($token){
            $baseUrl = config('app.base_url');
            $res = Http::withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$token
            ])->post($baseUrl.'api/logout');
            $decode = json_decode($res);
        }
        return redirect()->route('login');
    }

    public function authentication(Request $request){
        $baseUrl = config('app.base_url');
        $res = Http::withHeaders([
            'Accept' => 'application/json',
        ])->post($baseUrl.'api/login',[
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);
        $decode = json_decode($res);
        if($decode->status == "success"){
            $token = $decode->authorisation->token;
            $request->session()->put('token', $token);
            return redirect()->intended('/');
        }else{
            return redirect()->route('login')->with('status', 'Email or Password Incorrect!');;
        }
        
    }


    public function home(Request $request){
        $value = $request->session()->get('token');
        dd($value);
    }

    public function index(){
        return view('index');        
    }

    public function forget(Request $request){
        $request->session()->forget('token');
    }

    
}
