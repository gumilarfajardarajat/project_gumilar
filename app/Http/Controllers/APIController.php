<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getProjects(Request $request){
        $data = DB::table('tb_m_project as p')->join('tb_m_client as c','p.client_id','c.client_id');
        if($request->project_name){
            $data = $data->where('project_name','like','%'.$request->project_name.'%');
        }

        if($request->client_id){
            $data = $data->where('c.client_id',$request->client_id);
        }

        if($request->project_status){
            $data = $data->where('project_status',$request->project_status);
        }
        return response()->json(['data'=>$data->get()],200);
    }

    public function getClients(){
        $data = Client::all();

        return response()->json(['data'=>$data],200);
    }

    public function getProject($id){
        $data = Project::find($id);
        return response()->json(['data'=>$data],200);
    }

    public function updateProject(Request $request, $id){
        $data = Project::find($id);
        $data->project_name = $request->project_name;
        $data->client_id = $request->client_id;
        $data->project_status = $request->project_status;
        $data->project_start = $request->project_start;
        $data->project_end = $request->project_end;
        $data->save();
        return response()->json(['data'=>$data,],200);
    }

    public function storeProject(Request $request){
        $data = new Project();
        $data->project_name = $request->project_name;
        $data->client_id = $request->client_id;
        $data->project_status = $request->project_status;
        $data->project_start = $request->project_start;
        $data->project_end = $request->project_end;
        $data->save();
        return response()->json(['data'=>$data,],200);
    }

    public function destroyProject(Request $request){

        $listId = explode(',',$request->list);
        $data = Project::destroy($listId);
        return response()->json(['data'=>$data],200);
    }
}
