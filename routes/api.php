<?php

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;


Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
    Route::post('me', 'me');
});

Route::controller(APIController::class)->group(function () {
    Route::get('project', 'getProjects');
    Route::get('project/{id}', 'getProject');
    Route::post('project', 'storeProject');
    Route::delete('project', 'destroyProject');
    Route::put('project/{id}', 'updateProject');
    Route::get('client', 'getClients');
});


